import { Component, OnInit } from '@angular/core';
import { UsersService } from './../servicios/users.service' ;
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-posteo',
  templateUrl: './posteo.component.html',
  styleUrls: ['./posteo.component.css']
})
export class PosteoComponent implements OnInit {

  contenido: any;



  creoComment = {
    idUser: this.userservices.getID(),
    comment: '',
    idPost: this.userservices.getPostID()
  }

  constructor(private activatedRouted:ActivatedRoute, private userservices:UsersService) {

     this.activatedRouted.params.subscribe(params => {
      console.log("Parametro recibido: "+params['id']);

      
        this.userservices.getCommentsAPI(params['id'])
        .subscribe(data => {
          console.log("Recibo los comentarios")
          console.log(data.results);
          this.contenido = data;
        },
           error => {
             console.log("fallo el call de la API");
           
             console.log(error)
           });
  
           console.log(this.contenido);
    

    })


   }

  ngOnInit() {
  }

  agregar(forma: NgForm){
    this.userservices.postCommentAPI(this.creoComment)
    .subscribe(data => {
      console.log(data)
    },
    error => {
      console.log("Fallo el call de la API");
      console.log(error);

    });

  }

}
