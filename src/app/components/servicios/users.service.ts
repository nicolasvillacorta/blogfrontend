import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/Rx';


@Injectable()
export class UsersService {

    idUserLogeado: any;
    postID: any;
    postComments: any;
    contenido: any;

    private users = [];


    constructor(private http:Http) { 
    
    }

    setPostID(id:number){
      this.postID = id;
      console.log("Seteamos en servicios el id del post: "+id);
    }

    getPostID(){
      return this.postID;
    }

    setID(id: number){
      this.idUserLogeado = id;
      console.log("Seteamos en servicios el id de user: "+id);
    }

    getID(){
      return this.idUserLogeado;
    }

    getPostsAPI(){
      console.log("Usando getPostsAPI");
      
         let header = new Headers({'Content-Type':'application/json'});
         let postsURL = "http://192.168.101.61:8080/social/posts";
      
          return this.http.get(postsURL, {headers: header})
             .map(res =>{
               console.log(res.json());
               console.log("entro positivo REST getPostsAPI")
               this.users = res.json().results;
               return res.json();
             }, err => console.log("error: "+err.json()));
         }
    
    getCommentsAPI(id: number){
      console.log("Usando getCommentsAPI");

        let header = new Headers({'Content-Type':'application/json'});
        let commentsURL = "http://192.168.101.61:8080/social/comment/"+id;

        return this.http.get(commentsURL, {headers: header})
           .map(res =>{
             console.log(res.json());
             console.log("entro positivo REST getCommentsAPI")
             this.postComments = res.json().results;
             return res.json();
           }, err => console.log("error: "+err.json()));
    }

    getUsersAPI(){

        console.log("Usando getUsersAPI");
      
         let header = new Headers({'Content-Type':'application/json'});
         let usersURL = "http://192.168.101.61:8080/social/users";
      
          return this.http.get(usersURL, {headers: header})
             .map(res =>{
               console.log(res.json());
               console.log("entro positivo REST getUsersAPI")
               this.users = res.json().results;
               return res.json();
             }, err => console.log("error: "+err.json()));
         }

    postCommentAPI(comentario:any){ 

    console.log("Llamando a postCommentAPI");
  
      let header = new Headers({'Content-Type':'application/json'});
      let usersURL = "http://192.168.101.61:8080/social/comment";
      let body = comentario;
  
      return this.http.post(usersURL, body, {headers: header})
          .map(res =>{
            console.log(res.json());
            console.log("entro positivo REST postCommentAPI")
            // this.heroes = res.json().results;
            return res.json();
          }, err => console.log("error: "+err.json()));
      }
  

}