import { Component, OnInit } from '@angular/core';
import { UsersService } from '../servicios/users.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  users: any;

  constructor(private usersservice:UsersService, private router:Router) { }

  ngOnInit() {

  this.usersservice.getUsersAPI()
  .subscribe(data => {
    console.log(data)
    this.users = data.results;
  },
     error => {
       console.log("fallo el call de la API");
     
       console.log(error)
     });

    console.log(this.users);
  
  }

  logear(id: number){
    console.log("Recibi el id: "+id);
    this.router.navigate(['/posts']);
    this.usersservice.setID(id);
  }
}
