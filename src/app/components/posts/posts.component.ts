import { Component, OnInit } from '@angular/core';
import { UsersService } from './../servicios/users.service' ;
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router'

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  
  posts:any;

  constructor(private usersservice:UsersService, private router:Router) { }
  
  ngOnInit() {
    this.usersservice.getPostsAPI()
    .subscribe(data => {
      console.log(data)
      this.posts = data.results;
    },
       error => {
         console.log("fallo el call de la API");
       
         console.log(error)
       });
  
      console.log(this.posts);
  }

  entrarAlPost(id: number){
    this.router.navigate(['/post',id]);
    this.usersservice.setPostID(id);
  }

}
