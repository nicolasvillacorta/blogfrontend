import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { UsersService } from 'src/app/components/servicios/users.service';
import { AppComponent } from './app.component';
import { UsersComponent } from './components/users/users.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { appRouting } from './app.routes';
import { HomeComponent } from './components/home/home.component';
import { HttpModule } from '@angular/http';
import { PostsComponent } from './components/posts/posts.component';
import { PosteoComponent } from './components/posteo/posteo.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    NavbarComponent,
    HomeComponent,
    PostsComponent,
    PosteoComponent
  ],
  imports: [
    BrowserModule,
    appRouting,
    HttpModule,
    FormsModule
  ],
  providers: [UsersService],
  bootstrap: [AppComponent]
})
export class AppModule { }
