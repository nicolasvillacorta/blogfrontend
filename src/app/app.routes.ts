import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from 'src/app/components/home/home.component'
import { UsersComponent } from 'src/app/components/users/users.component'
import { PostsComponent } from './components/posts/posts.component';
import { PosteoComponent } from './components/posteo/posteo.component';

const routes: Routes = [
    { path: 'post/:id',component: PosteoComponent},
    { path: 'posts', component: PostsComponent},
    { path: 'users', component: UsersComponent},
    { path: 'home', component: HomeComponent },
    { path: '**', pathMatch:'full', redirectTo: 'home' }
];


export const appRouting = RouterModule.forRoot(routes);